﻿using System.ComponentModel.DataAnnotations;

namespace APIdemo.Models
{
    public class Pet
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        public string Name { get; set; }
      
        [Required]
        public string Breed { get; set; }

        [Required]
        public int Age { get; set; }

    }
}

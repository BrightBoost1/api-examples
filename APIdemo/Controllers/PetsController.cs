﻿using APIdemo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIdemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetsController : Controller
    {
        private static List<Pet> _pets = new List<Pet>
        {
            new Pet
            {
                Name = "Bobby",
                Breed = "Labradoodle",
                Age = 1,
                Id = 1
            },
            new Pet
            {
                Name = "Moogie",
                Breed = "Pretty cat",
                Age = 3,
                Id = 2
            },
            new Pet
            {
                Name = "Chloe",
                Breed = "Cockapoo",
                Age = 8,
                Id = 3
            }
        };

        [HttpGet]
        public IEnumerable<Pet> Get()
        {
            return _pets;
        }

        [HttpGet("{id}")]
        public Pet Get(int id)
        {
            return _pets.FirstOrDefault(p => p.Id == id);
        }

        [HttpPost]
        public ActionResult Post(Pet pet)
        {
            _pets.Add(pet);
            return Created("api/pet/" + pet.Id, pet);
        }

        [HttpPut("{id}")]
        public ActionResult Put(Pet pet, int id)
        {
            Pet p = _pets.FirstOrDefault(p => p.Id == id);
            Console.WriteLine("Found pet" + p.Name);
            if(p == null)
            {
                return NotFound();
            } 
            else
            {
                p.Breed = pet.Breed;
                p.Age = pet.Age;
                p.Name = pet.Name;
                return Ok();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            Console.WriteLine(id);
            Pet toBeDeleted = _pets.FirstOrDefault(p => p.Id == id);
           // Console.WriteLine("Found pet" + toBeDeleted.Name);

            if (toBeDeleted == null)
            {
                return NotFound();
            }
            else
            {
                _pets.Remove(toBeDeleted);
                return Ok();
            }
        }
    }
}

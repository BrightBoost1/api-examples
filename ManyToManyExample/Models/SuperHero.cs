﻿namespace ManyToManyExample.Models
{
    public class SuperHero
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SuperPower> SuperPowers { get; set; } = new();
    }
}

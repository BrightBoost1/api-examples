﻿namespace ManyToManyExample.Models
{
    public class SuperPower
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<SuperHero> SuperHeroes { get; set; } = new List<SuperHero>();
    }
}

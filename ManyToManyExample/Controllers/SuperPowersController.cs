﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ManyToManyExample.Models;

namespace ManyToManyExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperPowersController : ControllerBase
    {
        private readonly HeroDbContext _context;

        public SuperPowersController(HeroDbContext context)
        {
            _context = context;
        }

        // GET: api/SuperPowers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SuperPower>>> GetSuperPowers()
        {
          if (_context.SuperPowers == null)
          {
              return NotFound();
          }
            return await _context.SuperPowers.ToListAsync();
        }

        // GET: api/SuperPowers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SuperPower>> GetSuperPower(int id)
        {
          if (_context.SuperPowers == null)
          {
              return NotFound();
          }
            var superPower = await _context.SuperPowers.FindAsync(id);

            if (superPower == null)
            {
                return NotFound();
            }

            return superPower;
        }

        // PUT: api/SuperPowers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperPower(int id, SuperPower superPower)
        {
            if (id != superPower.Id)
            {
                return BadRequest();
            }

            _context.Entry(superPower).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperPowerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SuperPowers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SuperPower>> PostSuperPower(SuperPower superPower)
        {
          if (_context.SuperPowers == null)
          {
              return Problem("Entity set 'HeroDbContext.SuperPowers'  is null.");
          }
            _context.SuperPowers.Add(superPower);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSuperPower", new { id = superPower.Id }, superPower);
        }

        // DELETE: api/SuperPowers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperPower(int id)
        {
            if (_context.SuperPowers == null)
            {
                return NotFound();
            }
            var superPower = await _context.SuperPowers.FindAsync(id);
            if (superPower == null)
            {
                return NotFound();
            }

            _context.SuperPowers.Remove(superPower);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SuperPowerExists(int id)
        {
            return (_context.SuperPowers?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
